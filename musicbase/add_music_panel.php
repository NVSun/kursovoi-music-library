<div class = authpanel>
    
    <?php
    
    if (!empty($_SESSION['login']) && !empty($_SESSION['id'])) {
        if ($_GET['action'] == 'edit') {
           echo '<center>Редактирование записи</center>';
            $mode = 'update';
            $id = $_GET['index'];
            $currentResult = mysqli_query($db, "SELECT * FROM music WHERE id = ".$id); //извлекаем из базы все данные о пользователе с введенным логином
            if (!$currentResult) {
                printf("Error: %s\n", mysqli_error($db));
                exit();
            }
            $EditRow  =  mysqli_fetch_row($currentResult);

            $artist = $EditRow[1];
            $title = $EditRow[2];
            $album = $EditRow[3];
            $genre = $EditRow[4];
            $year = $EditRow[5];
        } else {
            echo '<center>Добавление нового трека</center>';
            $mode = 'add';
            $id = -1;
            $artist = '';
            $title = '';
            $album = '';
            $genre = '';
            $year = '';
        }
        
        echo '<form class = "form" action="/musiclist.php?action='.$mode.'" method="post">';
        echo '<input name = "index" type = hidden value = "'.$id.'">';
        echo '<div class = "formblock">
				<div class = "formcolumn">
				<div class = "answersBlock" style = "margin-bottom: 10px; margin-top: 5px">';
        echo '<center><div class = "formrow">
				<input name = "artist" style = "margin-left:0; padding-left:2px; " type = text size = 15 value = "'.$artist.'" placeholder = "Исполнитель" required></div></center>';
        echo '<center><div class = "formrow">
				<input name = "title" style = "margin-left:0; padding-left:2px; " type = text size = 15 value = "'.$title.'" placeholder = "Название" required></div></center>';
        echo '<center><div class = "formrow">
				<input name = "album" style = "margin-left:0; padding-left:2px; " type = text size = 15 value = "'.$album.'" placeholder = "Альбом"></div></center>';
        echo '<center><div class = "formrow">
				<input name = "genre" style = "margin-left:0; padding-left:2px; " type = text size = 15 value = "'.$genre.'" placeholder = "Жанр"></div></center>';
        echo '<center><div class = "formrow">
				<input name = "year" style = "margin-left:0; padding-left:2px; " type = text size = 15 value = "'.$year.'" placeholder = "Год" required></div></center>';
        echo '</div><center><input class = "submbutton" type = submit value = "'.($mode == 'add' ? 'Добавить' : 'Обновить').'"></center></div>';

        echo '</div></form>';

    } else {
        echo '<span class = error>Ошибка: вы не авторизованы. Доступ запрещён.</span>';
    }
    ?>
</div>