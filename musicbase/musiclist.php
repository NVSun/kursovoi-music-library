<?php require_once 'session-activator.php';
      include 'dbwork.php';
?>
<html>
<head>
    <title>Музыкальная библиотека</title>
    <?php
    include 'globalProperties.php';
    ?>
</head>
<body>
<div class = "page">
    <div class = "content">
        <?php
        $_SESSION['current_page'] = "musiclist.php";
        include 'header.php';
        include 'menu.php';?>
        <div class = "mainText">
            <nav class = "otchet">
                        <?php
                        if (!empty($_SESSION['login']) && !empty($_SESSION['id'])) {
                            if ($_GET['action'] == 'add') {

                                if (isset($_POST['artist']) && isset($_POST['title']) && isset($_POST['album']) && isset($_POST['genre']) && isset($_POST['year'])) {
                                    $artist = trim(htmlspecialchars(stripslashes($_POST['artist'])));
                                    $title = trim(htmlspecialchars(stripslashes($_POST['title'])));
                                    $album = trim(htmlspecialchars(stripslashes($_POST['album'])));
                                    $genre = trim(htmlspecialchars(stripslashes($_POST['genre'])));
                                    $year = trim(htmlspecialchars(stripslashes($_POST['year'])));

                                    $result2 = mysqli_query($db, 'INSERT INTO music (artist, title, album, genre, year, added_by) VALUES("' . $artist . '","' . $title . '","' . $album . '","' . $genre . '","' . $year . '","' . $_SESSION['login'] . '")');
                                    if (!$result2) {
                                        printf("Error: %s\n", mysqli_error($db));
                                        exit();
                                    }
                                    // Проверяем, есть ли ошибки
                                    if ($result2 == 'TRUE') {
                                        echo 'Трек успешно добавлен в библиотеку!<br>';
                                    } else {
                                        echo "<span class = error>Ошибка добавления трека</span><br>";
                                    }
                                } else {
                                    exit("Ошибка добавления трека в базу: отсутствует необходимая информация.");
                                }
                            } else if ($_GET['action'] == 'delete' && !empty($_GET['index'])) {
                                $index = trim(htmlspecialchars(stripslashes($_GET['index'])));
                                $delete_result = mysqli_query($db, 'DELETE FROM music WHERE id = "'.$index.'"');
                                if ($delete_result) {
                                    echo 'Трек успешно удалён из библиотеки!<br>';
                                } else {
                                    printf("Error: %s\n", mysqli_error($db));
                                    exit();
                                }
                            } else if ($_GET['action'] == 'update') {
                                    if (isset($_POST['artist']) && isset($_POST['title']) && isset($_POST['album']) && isset($_POST['genre']) && isset($_POST['year']) && isset($_POST['index'])) {
                                        $artist = trim(htmlspecialchars(stripslashes($_POST['artist'])));
                                        $title = trim(htmlspecialchars(stripslashes($_POST['title'])));
                                        $album = trim(htmlspecialchars(stripslashes($_POST['album'])));
                                        $genre = trim(htmlspecialchars(stripslashes($_POST['genre'])));
                                        $year = trim(htmlspecialchars(stripslashes($_POST['year'])));
                                        $id = trim(htmlspecialchars(stripslashes($_POST['index'])));

                                        $result2 = mysqli_query($db, 'UPDATE music SET artist = "' . $artist . '", title = "' . $title . '", album = "' . $album . '", genre = "' . $genre . '", year = "' . $year . '" WHERE id = '.$id);
                                        if (!$result2) {
                                            printf("Error: %s\n", mysqli_error($db));
                                            exit();
                                        }
                                        // Проверяем, есть ли ошибки
                                        if ($result2 == 'TRUE') {
                                            echo 'Трек успешно отредактирован!<br>';
                                        } else {
                                            echo "<span class = error>Ошибка добавления трека</span><br>";
                                        }
                                    } else {
                                        exit("Ошибка добавления трека в базу: отсутствует необходимая информация.");
                                    }
                            }
                        }
                        echo '<span>'.
                            '<table>';
                        echo ' <tr class = titlerow><th>Исполнитель<a class = "sortbutton" href = musiclist.php?sort=artist>↑</a><a class = "sortbutton" href = musiclist.php?sort=artist&order=reversed>↓</a></th><th>Название<a class = "sortbutton" href = musiclist.php?sort=title>↑</a><a class = "sortbutton" href = musiclist.php?sort=title&order=reversed>↓</a></th><th>Альбом<a class = "sortbutton" href = musiclist.php?sort=album>↑</a><a class = "sortbutton" href = musiclist.php?sort=album&order=reversed>↓</a></th><th>Жанр<a class = "sortbutton" href = musiclist.php?sort=genre>↑</a><a class = "sortbutton" href = musiclist.php?sort=genre&order=reversed>↓</a></th><th>Год<a class = "sortbutton" href = musiclist.php?sort=year>↑</a><a class = "sortbutton" href = musiclist.php?sort=year&order=reversed>↓</a></th></tr>';

                        if (isset($_GET['sort']) && (strcmp($_GET['sort'] , 'artist') || strcmp($_GET['sort'], 'title') ||  strcmp($_GET['sort'], 'album') ||  strcmp($_GET['sort'], 'genre') ||  strcmp($_GET['sort'], 'year')) ) {
                            $sort = $_GET['sort'];
                        } else {
                            $sort='artist';
                        }
                        if ($_GET['order'] == 'reversed') {
                            $order = ' DESC';
                        } else {
                            $order = '';
                        }
                        $result = mysqli_query($db, "SELECT * FROM music ORDER BY ".$sort.$order); //извлекаем из базы все данные о пользователе с введенным логином
                        if (!$result) {
                            printf("Error: %s\n", mysqli_error($db));
                            exit();
                        }
                        include "searchpanel.php";
                        if ($_GET["action"] == 'search') {
                            $query = trim(htmlspecialchars(stripslashes($_GET['query'])));
                            if ($query != '') {
                                echo '<span>Результаты поиска:</span>';
                            } else {
                                $_GET["action"] = '';
                            }
                        }
                        while ($row  =  mysqli_fetch_row($result)) {
                            if ($_GET["action"] == 'search') {
                                for ($i = 1; $i <= 5; $i++) {
                                    if (stripos($row[$i], $query) !== FALSE) {
                                        echo '<tr class = tablerow><td>' . $row[1] . '</td><td>' . $row[2] . '</td><td>' . $row[3] . '</td><td>' . $row[4] . '</td><td>' . $row[5] . '</td>' . (!empty($_SESSION['login']) && !empty($_SESSION['id']) ? '<td><a class = "sortbutton" href = musiclist.php?action=delete&index=' . $row[0] . '>x</a></td><td><a class = "sortbutton" href = musiclist.php?action=edit&index=' . $row[0] . '>✎</a></td>' : '') . '</tr>';
                                        break;
                                    }
                                }
                            } else {
                                echo '<tr class = tablerow><td>' . $row[1] . '</td><td>' . $row[2] . '</td><td>' . $row[3] . '</td><td>' . $row[4] . '</td><td>' . $row[5] . '</td>' . (!empty($_SESSION['login']) && !empty($_SESSION['id']) ? '<td><a class = "sortbutton" href = musiclist.php?action=delete&index=' . $row[0] . '>x</a></td><td><a class = "sortbutton" href = musiclist.php?action=edit&index=' . $row[0] . '>✎</a></td>' : '') . '</tr>';
                            }
                        }
                        echo '</table></span><br><span>';
                        if (!empty($_SESSION['login']) && !empty($_SESSION['id'])) {
                            include "add_music_panel.php";
                        } else {
                            echo 'Для добавления трека в библиотеку треков вы должны быть авторизованы!';
                        }
                        echo '</span>';
                        ?>

            </nav>
        </div>
    </div>
</div>
</body>
</html>