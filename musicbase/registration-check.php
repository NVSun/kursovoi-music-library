<?php require_once 'session-activator.php'; ?>
<html>
	<head>
		<title>Регистрация</title>
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<div class = "page">
			<div class = "content">
				<?php 
				include 'header.php';
				include 'menu.php';?>
				<div class = "mainText"> 
					<?php
						if (isset($_POST['login'])) { $login = $_POST['login']; if ($login == '') { unset($login);} } //заносим введенный пользователем логин в переменную $login, если он пустой, то уничтожаем переменную
						if (isset($_POST['password'])) { $password=$_POST['password']; if ($password =='') { unset($password);} }
						//заносим введенный пользователем пароль в переменную $password, если он пустой, то уничтожаем переменную
					 if (empty($login) or empty($password)) //если пользователь не ввел логин или пароль, то выдаем ошибку и останавливаем скрипт
						{
						exit ("Вы ввели не всю информацию, вернитесь назад и заполните все поля!");
						}
						//если логин и пароль введены, то обрабатываем их, чтобы теги и скрипты не работали, мало ли что люди могут ввести
						$login = stripslashes($login);
						$login = htmlspecialchars($login);
					 $password = stripslashes($password);
						$password = htmlspecialchars($password);
					 //удаляем лишние пробелы
						$login = trim($login);
						$password = trim($password);
					 // подключаемся к базе
						include ("dbwork.php");// файл dbwork.php должен быть в той же папке, что и все остальные, если это не так, то просто измените путь 
					 // проверка на существование пользователя с таким же логином
						$result = mysqli_query($db, 'SELECT id FROM users WHERE login="'.$login.'"');
                        if (!$result) {
                            printf("Error: %s\n", mysqli_error($db));
                            exit();
                        }
						$myrow = mysqli_fetch_array($result);
						if (!empty($myrow['id'])) {
						exit ("Извините, введённый вами логин уже зарегистрирован. Введите другой логин.");
						}
					 // если такого нет, то сохраняем данные
						$result2 = mysqli_query($db, 'INSERT INTO users (login, password) VALUES("'.$login.'","'.$password.'")');
                        if (!$result2) {
                            printf("Error: %s\n", mysqli_error($db));
                            exit();
                        }
						// Проверяем, есть ли ошибки
						if ($result2=='TRUE')
						{
                            echo "Вы были успешно зарегистрированы!";
                            $_SESSION['current_page'] = "registration";
							include 'authcheck.php';
						} else {
						    echo "Ошибка! Вы не зарегистрированы.";
						}
					?>
				</div>
			</div>
		</div>
	</body>
</html>

