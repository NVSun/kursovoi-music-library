<?php require_once 'session-activator.php'; ?>
<html>
	<head>
        <title>Музыкальная библиотека</title>
        <?php
            include 'globalProperties.php';
        ?>
	</head>
	<body>
		<div class = "page">
			<div class = "content">
				<?php
                $_SESSION['current_page'] = 'index.php';
				include 'header.php';
				include 'menu.php';?>
				<div class = "mainText"> 
					<nav class = "otchet">
						<span>Данный сайт представляет собой музыкальную библиотеку, в которой содержится информация по добавленным пользователями композициям.</span>
					</nav>
				</div>
			</div>
		</div>
	</body>
</html>